import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'

const Pwd = props => (
  <div>
    Greetings, {props.user}! This is the "{props.action}" page of <a href={'/' + props.controller}>`{props.controller}`</a>.
  </div>
)

Pwd.defaultProps = {
  controller: window.controller,
  action: window.action,
  user: 'User'
}

Pwd.propTypes = {
  controller: PropTypes.string,
  action: PropTypes.string,
  user: PropTypes.string
}

document.addEventListener(
  'DOMContentLoaded',
  () => {
    ReactDOM.render(
      <Pwd
        user="visitor"
      />,
      document.body.appendChild(
        document.createElement('div')
      ),
    )
  }
)
