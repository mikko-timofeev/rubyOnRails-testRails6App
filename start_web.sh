#!/bin/sh

rm -f tmp/pids/server.pid

echo 'waiting for build to finish...'
while [ $(test -f .building) ]; do
  sleep 1
done

source /home/${LOCAL_USER}/.bashrc
bundle exec rails s -p 3000 -b '0.0.0.0'
