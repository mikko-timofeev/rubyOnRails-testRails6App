FROM bitnami/minideb

ENV TZ=Europe/Moscow
ENV DEBIAN_FRONTEND noninteractive
ENV PHANTOMJS_VERSION 2.1.1
ENV CONFIGURE_OPTS --disable-install-doc

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
    && install_packages wget gnupg2 \
    && wget --no-check-certificate -qO - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list && echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg-testing main 13" > /etc/apt/sources.list.d/pgdg-testing.list \
    && apt-get update && apt-get install -y --allow-unauthenticated postgresql-client-13 libpq-dev
RUN install_packages bzip2 git make g++ clang build-essential libsodium-dev ca-certificates libmariadbclient-dev curl zlib1g-dev libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libmagickwand-dev librsvg2-bin librsvg2-dev jpegoptim curl ssh chromium imagemagick \
    && curl -L "https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2" | tar xj \
    && mkdir -p /root/.phantomjs/$PHANTOMJS_VERSION \
    && mv phantomjs-$PHANTOMJS_VERSION-linux-x86_64 /root/.phantomjs/$PHANTOMJS_VERSION/x86_64-linux \
    && ln -sf /root/.phantomjs/$PHANTOMJS_VERSION/x86_64-linux/bin/phantomjs /usr/local/bin \
    && mkdir /app
RUN curl https://deb.nodesource.com/setup_12.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y nodejs yarn

WORKDIR /app

ARG LOCAL_USER=root
ARG LOCAL_GROUP=root
ARG LOCAL_UID=0
ARG LOCAL_GID=0
ARG SSH_PRIVATE_KEY_NAME
ARG SSH_PRIVATE_KEY
ARG SSH_CONFIG
ARG RUBY_VERSION
ARG GIT_MAIL
ARG GIT_NAME

RUN groupadd -g $LOCAL_GID -f $LOCAL_GROUP || true \
    && useradd -u $LOCAL_UID -g $LOCAL_GROUP -m $LOCAL_USER || true \
    && chown $LOCAL_USER:$LOCAL_GROUP /app \
    && cp -r /root/.phantomjs /home/$LOCAL_USER/.phantomjs || true

RUN git clone https://github.com/sstephenson/rbenv.git /home/$LOCAL_USER/.rbenv
RUN git clone https://github.com/sstephenson/ruby-build.git /home/$LOCAL_USER/.rbenv/plugins/ruby-build

RUN /home/$LOCAL_USER/.rbenv/plugins/ruby-build/install.sh
ENV PATH /home/$LOCAL_USER/.rbenv/bin:/home/$LOCAL_USER/.rbenv/shims:$PATH
RUN echo "export PATH=\"$PATH:$HOME/.rbenv/bin:`yarn global bin`\"" >> /home/$LOCAL_USER/.bashrc \
    && echo 'eval "$(rbenv init -)"' >> /home/$LOCAL_USER/.bashrc \
    && echo 'eval "$(ssh-agent -s)"' >> /home/$LOCAL_USER/.bashrc \
    && /bin/bash -c "source /home/${LOCAL_USER}/.bashrc"

RUN chown $LOCAL_USER:$LOCAL_GROUP /home/$LOCAL_USER/.rbenv
USER $LOCAL_USER
RUN rbenv install $RUBY_VERSION \
    && rbenv global $RUBY_VERSION

RUN /bin/bash -c "\
    cd /home/$LOCAL_USER \
    && mkdir -p ./.ssh \
    && cd ./.ssh \
    && umask 0077 \
    && gem install bundler -v '~> 2.2.18' --force \
    && gem install haml_lint \
    "
# COPY --chown=$LOCAL_USER:$LOCAL_GROUP Gemfile* ./
# RUN /bin/bash -c " \
#     bundle install --jobs 4 --retry 4 \
#     && echo && echo '-------------' && cat /app/Gemfile.lock \
#     "
